package com.sotatek.ssac.networkManager;

public class NoAvailableNetworkInterfaceException extends Exception{
	public String getMessage() {
		return "No Available Network Interface";
	}
}
