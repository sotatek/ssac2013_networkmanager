package com.sotatek.ssac.networkManager;

public class DataEvent extends Event{
	byte[] data;
	String node;
	String type;
	DataEvent(byte[]data, String node, String type) {
		this.data = data;
		this.node = node;
		this.type = type;
	}
}
