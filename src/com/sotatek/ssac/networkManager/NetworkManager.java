package com.sotatek.ssac.networkManager;

import java.util.ArrayList;
import java.util.List;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.samsung.chord.ChordManager;
import com.samsung.chord.IChordChannel;
import com.sotatek.ssac.networkManager.ChordService.ChordServiceBinder;
import com.sotatek.ssac.networkManager.Event.EventType;

public class NetworkManager extends ListenerAdapter{
	static final long SHARE_FILE_TIMEOUT_MILISECONDS = 1000 * 60 * 5;
	private static final String TAG = "Network";
	
	private Context mContext;
	private String mIntentStart, mIntentBind, mIntentStop;
	private ChordService mChordService;
	private ChordManager mChordManager;
	private IChordChannel mAppChannel;
	private boolean isStartingService;
	private ArrayList<GeneralListener> mListeners = new ArrayList<GeneralListener>();
	private ServiceConnection mConnection = new ServiceConnection() {
		 @Override
	        public void onServiceConnected(ComponentName name, IBinder service) {
	            ChordServiceBinder binder = (ChordServiceBinder)service;
	            ChordService chordService = binder.getService();
	            try {
	                chordService.addListener(NetworkManager.this);
	            	for(GeneralListener listener: mListeners) {
	            	    chordService.addListener(listener);
	            	}
	            	chordService.initializeChord();
	                int error = chordService.start();
	                if(error != ChordManager.ERROR_NONE) {
	                    chordService.dispatchEvent(new NetworkEvent(EventType.NETWORK_JOIN_FAILED));
	                }
	                
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	            mChordService = chordService;
	            isStartingService = false;
	        }

	        @Override
	        public void onServiceDisconnected(ComponentName name) {
	            isStartingService = false;
	            mChordService = null;
	            mAppChannel = null;
	        }
	};
	
	public NetworkManager(Context context, String serviceStartIntent, String serviceBindIntent, String serviceStopIntent) {
		mContext = context;
		mIntentStart = serviceStartIntent;
		mIntentBind = serviceBindIntent;
		mIntentStop = serviceStopIntent;
	}
	
	public void joinNetwork() {
		if(mChordService == null && !isStartingService) {
		    isStartingService = true;
			//start service
			Intent intent = new Intent(mIntentStart);
			mContext.startService(intent);
			
			//bind service
			intent = new Intent(mIntentBind);
	        mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
		} else if(mChordManager != null && mAppChannel == null) {
		    mChordService.start();
		}
		
	}
	
	public void leaveNetwork() {
		if(mChordManager != null) {
			mChordManager.leaveChannel(ChordService.CHORD_APPLICATION_CHANNEL);
			mChordManager.stop();
			mAppChannel = null;
			mChordService.dispatchEvent(new NetworkEvent(EventType.NETWORK_LEFT));
		}
	}
	
	
	@Override
	public final void onNetworkJoined() {
		mAppChannel = mChordService.getAppChannel();
		mChordManager = mChordService.getChordManager();
	}
	
	public List<String> getJoinedNodeNames() {
		return mAppChannel.getJoinedNodeList();
	}
	
	 // Requests for nodes on the channel.
    public List<String> getJoinedNodeList(String channelName) {
        //Log.d(TAG, "getJoinedNodeList()");
        // Request the channel interface for the specific channel name.
        IChordChannel channel = mChordManager.getJoinedChannel(channelName);
        if (null == channel) {
            //Log.e(TAG, "getJoinedNodeList() : invalid channel instance-" + channelName);
            return null;
        }

        return channel.getJoinedNodeList();
    }
    
	public void addListener(GeneralListener listener) {
		if(mChordService != null) {
			mChordService.addListener(listener);
		}
		if(!mListeners.contains(listener)){
			mListeners.add(listener);
		}
	}
	
	public String getChannelName() {
		return mAppChannel.getName();
	}
	
	public String getNodeName() {
		return mChordManager.getName();
	}
	
	/**
	 * 
	 * @return exchangeId used to cancel file
	 */
	public String sendFile(String toNode, String filePath, String fileType) {
		return mChordService.sendFile(toNode, filePath, fileType);
	}
	
	// Accept to receive file.
    public boolean acceptFile(String exchangeId) {
        return mAppChannel.acceptFile(exchangeId, 30*1000, 2, 300 * 1024);
    }
    
    // Cancel file transfer while it is in progress.
    public boolean cancelFile(String exchangeId) {
        return mAppChannel.cancelFile(exchangeId);
    }

    // Reject to receive file.
    public boolean rejectFile(String exchangeId) {
        return mChordService.rejectFile(exchangeId);
    }
    
    public boolean sendData(byte[] buf, String nodeName, String type) {
        if (nodeName == null) {
            //Log.v("Network", "sendData : NODE Name IS NULL !!");
            return false;
        }

        byte[][] payload = new byte[1][];
        payload[0] = buf;

        /*
         * @param toNode The joined node name that the message is sent to. It is
         * mandatory.
         * @param payloadType User defined message type. It is mandatory.
         * @param payload The package of data to send
         * @return Returns true when file transfer is success. Otherwise, false
         * is returned
         */
        if (false == mAppChannel.sendData(nodeName, type, payload)) {
            //Log.e(TAG, "sendData : fail to sendData");
            return false;
        }

        return true;
    }
    
    public boolean broadCastData(byte[] buf, String type) {
    	 byte[][] payload = new byte[1][];
         payload[0] = buf;

         /*
          * @param toNode The joined node name that the message is sent to. It is
          * mandatory.
          * @param payloadType User defined message type. It is mandatory.
          * @param payload The package of data to send
          * @return Returns true when file transfer is success. Otherwise, false
          * is returned
          */
         if (false == mAppChannel.sendDataToAll(type, payload)) {
             //Log.e(TAG, "sendData : fail to sendData");
             return false;
         }

         return true;
    }
    
    // Send data message to the all nodes on the channel.
    public boolean sendDataToAll(byte[] buf, String type) {
        byte[][] payload = new byte[1][];
        payload[0] = buf;

        /*
         * @param payloadType User defined message type. It is mandatory.
         * @param payload The package of data to send
         * @return Returns true when file transfer is success. Otherwise, false
         * is returned.
         */
        if (false == mAppChannel.sendDataToAll(type, payload)) {
            //Log.e(TAG, "sendDataToAll : fail to sendDataToAll");
            return false;
        }

        return true;
    }
    
    /**
     * Set a keep-alive timeout. Node has keep-alive timeout. The timeoutMsec
     * determines the maximum keep-alive time to wait to leave when there is no
     * data from the nodes. Default time is 15000 millisecond.
     */
    public void setNodeKeepAliveTimeout(long timeoutMsec) {
        mChordManager.setNodeKeepAliveTimeout(timeoutMsec);
    }
    
    // Get an IPv4 address that the node has.
    public String getNodeIpAddress(String nodeName) {
       return mAppChannel.getNodeIpAddress(nodeName);
    }
    
    public void stopService() {
        if(mChordService != null) {
        	mContext.unbindService(mConnection);
    		mChordService = null;
    		isStartingService = false;
    		Intent intent = new Intent(mIntentStop);
    		mContext.stopService(intent);
        }
    }
}
