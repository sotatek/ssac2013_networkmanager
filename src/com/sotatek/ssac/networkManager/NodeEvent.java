package com.sotatek.ssac.networkManager;


public class NodeEvent extends Event{
	String node, channel;
	public NodeEvent(EventType type, String node, String channel) {
		this.type = type;
		this.node = node;
		this.channel = channel;
	}
}
