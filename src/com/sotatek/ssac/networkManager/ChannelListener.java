package com.sotatek.ssac.networkManager;

public interface ChannelListener extends GeneralListener {
	public static final int SENT = 0;

	public static final int RECEIVED = 1;

	public static final int CANCELLED = 2;

	public static final int REJECTED = 3;

	public static final int FAILED = 4;

	void onReceiveData(String node, byte[] data, String type);

	void onFileWillReceive(FileEvent event);

	void onFileProgress(FileEvent event);

	void onFileCompleted(FileEvent event);

	void onNodeJoined(String node);
	
	void onNodeLeft(String node);
}
