package com.sotatek.ssac.networkManager;


public class ListenerAdapter implements NetworkListener, ChannelListener{

	@Override
	public void onReceiveData(String node, byte[]data, String type) {
		
		
	}

	@Override
	public void onFileWillReceive(FileEvent event) {
		
		
	}

	@Override
	public void onFileProgress(FileEvent event) {
		
		
	}

	@Override
	public void onFileCompleted(FileEvent event) {
		
		
	}

	@Override
	public void onNodeJoined(String node) {
		
		
	}
	
	@Override
	public void onNodeLeft(String node) {
		
	}

	@Override
	public void onNetworkJoined() {
		
		
	}

	@Override
	public void onNetworkJoinFailed() {
		
		
	}

	@Override
	public void onNetworkDisconnected() {
		
		
	}

	@Override
	public void onReconnected() {
		
		
	}

	@Override
	public void onUnexpectedStop() {
		
		
	}

    @Override
    public void onNetworkLeft() {
        
    }
	

}
