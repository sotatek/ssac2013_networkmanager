package com.sotatek.ssac.networkManager;

public interface NetworkListener extends GeneralListener{
    void onNetworkJoined();
    
    void onNetworkJoinFailed();
    
    void onNetworkDisconnected();
    
    void onReconnected();
    
    void onUnexpectedStop();
    
    void onNetworkLeft();
}
