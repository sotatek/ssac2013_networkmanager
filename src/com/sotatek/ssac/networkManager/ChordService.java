package com.sotatek.ssac.networkManager;

import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.StatFs;

import com.samsung.chord.ChordManager;
import com.samsung.chord.ChordManager.INetworkListener;
import com.samsung.chord.IChordChannel;
import com.samsung.chord.IChordChannelListener;
import com.samsung.chord.IChordManagerListener;
import com.sotatek.ssac.networkManager.Event.EventType;

public class ChordService extends Service implements IChordChannelListener {
    public static String CHORD_FILE_PATH = Environment
            .getExternalStorageDirectory().getAbsolutePath() + "/TapSiriusFiles/Temp";
    public static String CHORD_APPLICATION_CHANNEL = "com.sotatek.tapsirius";
    private static final String TAG = "Network";
    private static final String TAGClass = "ChordService : ";

    private ChordManager mChordManager;
    private IChordChannel mAppChannel;
    private ArrayList<GeneralListener> mListeners = new ArrayList<GeneralListener>();
    private Hashtable<String, String> fileTypes = new Hashtable<String, String>();

    private PowerManager.WakeLock mWakeLock = null;

    private final IBinder mBinder = new ChordServiceBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        //Log.d(TAG, TAGClass + "onCreate()");
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        //Log.d(TAG, TAGClass + "onDestroy()");
        super.onDestroy();
        try {
            release();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRebind(Intent intent) {
        //Log.d(TAG, TAGClass + "onRebind()");
        super.onRebind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
       // Log.d(TAG, TAGClass + "onStartCommand()");
        return super.onStartCommand(intent, START_NOT_STICKY, startId);
    }

    @Override
    public boolean onUnbind(Intent intent) {
       // Log.d(TAG, TAGClass + "onUnbind()");
        return super.onUnbind(intent);
    }

    public void initializeChord() {
        if (mChordManager != null)
            return;
        mChordManager = ChordManager.getInstance(this);
        //keep alive for 5 hours by default
        mChordManager.setNodeKeepAliveTimeout(5 * 3600 * 1000);
        mChordManager.setTempDirectory(CHORD_FILE_PATH);
        mChordManager.setHandleEventLooper(getMainLooper());
        mChordManager.setNetworkListener(new INetworkListener() {

            @Override
            public void onDisconnected(int arg0) {

            }

            @Override
            public void onConnected(int arg0) {
                start();
            }
        });
    }

    // Start chord
    public int start() {
        try {
            int interfaceType = getAvailableNetworkInterface();
            acqureWakeLock();
            return mChordManager.start(interfaceType,
                    new IChordManagerListener() {

                        @Override
                        public void onStarted(String name, int reason) {
                            if (reason == STARTED_BY_USER) {
                                mAppChannel = mChordManager.joinChannel(
                                        CHORD_APPLICATION_CHANNEL,
                                        ChordService.this);
                                if (mAppChannel != null) {
                                    dispatchEvent(new NetworkEvent(
                                            EventType.NETWORK_JOINED));
                                }
                            } else if (reason == STARTED_BY_RECONNECTION) {
                                mAppChannel = mChordManager.joinChannel(
                                        CHORD_APPLICATION_CHANNEL,
                                        ChordService.this);
                                if (mAppChannel != null) {
                                    dispatchEvent(new NetworkEvent(
                                            EventType.NETWORK_RECONNECTED));
                                }
                            }

                            if (mAppChannel == null) {
                                dispatchEvent(new NetworkEvent(
                                        EventType.NETWORK_JOIN_FAILED));
                            }
                        }

                        @Override
                        public void onNetworkDisconnected() {
                            dispatchEvent(new NetworkEvent(
                                    EventType.NETWORK_DISCONNECTED));
                        }

                        @Override
                        public void onError(int error) {
                            if (error == ERROR_UNEXPECTED_STOP) {
                                dispatchEvent(new NetworkEvent(
                                        EventType.NETWORK_STOP_UNEXPECTED));
                            }
                        }
                    });
        } catch (NoAvailableNetworkInterfaceException e) {
            return IChordManagerListener.ERROR_START_FAIL;
        }

    }

    private int getAvailableNetworkInterface()
            throws NoAvailableNetworkInterfaceException {
        if (mChordManager == null) {
            throw new NoAvailableNetworkInterfaceException();
        }
        List<Integer> interfaces = mChordManager.getAvailableInterfaceTypes();
        boolean bAvailableMobileAp = false;
        boolean bAvailableWifiDirect = false;

        for (int interfaceValue : interfaces) {
            if (interfaceValue == ChordManager.INTERFACE_TYPE_WIFI) {
                return interfaceValue;
            }

            if (interfaceValue == ChordManager.INTERFACE_TYPE_WIFIAP) {
                bAvailableMobileAp = true;
            } else if (interfaceValue == ChordManager.INTERFACE_TYPE_WIFIP2P) {
                bAvailableWifiDirect = true;
            }
        }

        if (bAvailableMobileAp) {
            return ChordManager.INTERFACE_TYPE_WIFIAP;
        }
        if (bAvailableWifiDirect) {
            return ChordManager.INTERFACE_TYPE_WIFIP2P;
        }

        throw new NoAvailableNetworkInterfaceException();
    }

    private void acqureWakeLock() {
        if (null == mWakeLock) {
            PowerManager powerMgr = (PowerManager) getSystemService(Context.POWER_SERVICE);
            mWakeLock = powerMgr.newWakeLock(PowerManager.FULL_WAKE_LOCK,
                    "ChordApiDemo Lock");
        }

        if (mWakeLock.isHeld()) {
            mWakeLock.release();
        }

        mWakeLock.acquire();
    }

    private void releaseWakeLock() {
        if (null != mWakeLock && mWakeLock.isHeld()) {
            mWakeLock.release();
        }
    }

    public class ChordServiceBinder extends Binder {

        public ChordService getService() {
            return ChordService.this;
        }

    }

    // Release chord
    public void release() throws Exception {
        if (mChordManager != null) {
            releaseWakeLock();
            mChordManager.stop();
            mChordManager.setNetworkListener(null);
            mChordManager = null;
            //Log.d(TAG, "[UNREGISTER] Chord unregistered");
        }

    }

    IChordChannel getAppChannel() {
        return mAppChannel;
    }

    ChordManager getChordManager() {
        return mChordManager;
    }

    public void addListener(GeneralListener listener) {
        if (!mListeners.contains(listener)) {
            mListeners.add(listener);
        }
    }

    public void dispatchEvent(Event event) {
        for (GeneralListener listener : mListeners) {
            if (event instanceof NetworkEvent) {
                NetworkListener networkListener = (NetworkListener) listener;
                switch (event.type) {
                case NETWORK_JOINED:
                    networkListener.onNetworkJoined();
                    break;
                case NETWORK_LEFT:
                    networkListener.onNetworkLeft();
                    break;
                case NETWORK_JOIN_FAILED:
                    networkListener.onNetworkJoinFailed();
                    break;
                case NETWORK_DISCONNECTED:
                    networkListener.onNetworkDisconnected();
                    break;
                case NETWORK_RECONNECTED:
                    networkListener.onReconnected();
                    break;
                case NETWORK_STOP_UNEXPECTED:
                    networkListener.onUnexpectedStop();
                    break;
                }
            }
            if (event instanceof FileEvent) {
                ChannelListener channelListener = (ChannelListener) listener;
                FileEvent fileEvent = (FileEvent) event;
                switch (event.type) {
                case FILE_COMPLETED:
                    channelListener.onFileCompleted(fileEvent);
                    break;
                case FILE_PROGRESS:
                    channelListener.onFileProgress(fileEvent);
                    break;
                case FILE_WILL_RECEIVE:
                    channelListener.onFileWillReceive(fileEvent);
                    break;
                }
            } else if (event instanceof NodeEvent) {
                ChannelListener channelListener = (ChannelListener) listener;
                NodeEvent nodeEvent = (NodeEvent) event;
                switch (event.type) {
                case NODE_JOINED:
                    channelListener.onNodeJoined(nodeEvent.node);
                    break;
                case NODE_LEFT:
                    channelListener.onNodeLeft(nodeEvent.node);
                    break;
                }
            } else if (event instanceof DataEvent) {
                ChannelListener channelListener = (ChannelListener) listener;
                DataEvent dataEvent = (DataEvent) event;
                channelListener.onReceiveData(dataEvent.node, dataEvent.data, dataEvent.type);
            }

        }
    }

    @Override
    public void onDataReceived(String fromNode, String fromChannel,
            String type, byte[][] data) {
        if(fromChannel.equals(CHORD_APPLICATION_CHANNEL)) {
            dispatchEvent(new DataEvent(data[0], fromNode, type));
        }
    }

    @Override
    public void onFileChunkReceived(String fromNode, String fromChannel,
            String fileName, String hash, String fileType, String exchangeId,
            long fileSize, long offset) {
        if(fromChannel.equals(CHORD_APPLICATION_CHANNEL)) {
            FileEvent fileEvent = new FileEvent(EventType.FILE_PROGRESS, fromNode, fileName, fileType, exchangeId);
            fileEvent.progress = (int) (offset * 100 / fileSize);
            dispatchEvent(fileEvent);
        }
    }

    @Override
    public void onFileChunkSent(String toNode, String toChannel,
            String fileName, String hash, String fileType, String exchangeId,
            long fileSize, long offset, long chunkSize) {
        if(toChannel.equals(CHORD_APPLICATION_CHANNEL)) {
            FileEvent fileEvent = new FileEvent(EventType.FILE_PROGRESS, toNode, fileName, fileType, exchangeId);
            fileEvent.progress = (int) (offset * 100 / fileSize);
            dispatchEvent(fileEvent);
        }
    }

    @Override
    public void onFileFailed(String node, String channel, String fileName,
            String hash, String exchangeId, int reason) {
        if(channel.equals(CHORD_APPLICATION_CHANNEL)) {
            FileEvent fileEvent = new FileEvent(EventType.FILE_COMPLETED, node, fileName, fileTypes.get(exchangeId), exchangeId);
            switch (reason) {
            case ERROR_FILE_REJECTED: {
                fileEvent.reason = ChannelListener.REJECTED;
                break;
            }

            case ERROR_FILE_CANCELED: {
                fileEvent.reason = ChannelListener.CANCELLED;
                break;
            }
            case ERROR_FILE_CREATE_FAILED:
            case ERROR_FILE_NO_RESOURCE:
            default:
                fileEvent.reason = ChannelListener.FAILED;
                break;
            }
            dispatchEvent(fileEvent);
        }
    }

    protected String selectFolder(String fileType) {
        return CHORD_FILE_PATH;
    }

    protected String selectFileName(String fileType, String trimmedFileName) {
        return trimmedFileName;
    }

    @Override
    public void onFileReceived(String fromNode, String fromChannel, String fileName,
            String hash, String fileType, String exchangeId, long fileSize, String tmpFilePath) {
        if(fromChannel.equals(CHORD_APPLICATION_CHANNEL)) {
            String savedName = fileName;

            int i = savedName.lastIndexOf(".");
            String name = i > -1 ? savedName.substring(0, i) : savedName;
            String ext = i > -1 ? savedName.substring(i) : "";

            String dirName = selectFolder(fileType);
            String finalFileName = selectFileName(fileType, savedName);
            File targetFile = new File(dirName, finalFileName);
            int index = 0;
            while (targetFile.exists()) {
                savedName = name + "_" + index + ext;
                targetFile = new File(selectFolder(fileType), savedName);

                index++;

                //Log.d(TAG, TAGClass + "onFileReceived : " + savedName);
            }

            File srcFile = new File(tmpFilePath);
            srcFile.renameTo(targetFile);
            dispatchEvent(new FileEvent(EventType.FILE_COMPLETED, fromNode, fromChannel, targetFile.getAbsolutePath(), fileType, exchangeId, ChannelListener.RECEIVED));
        }
    }

    @Override
    public void onFileSent(String toNode, String toChannel, String fileName, String hash,
            String fileType, String exchangeId) {
        if(toChannel.equals(CHORD_APPLICATION_CHANNEL)) {
            dispatchEvent(new FileEvent(EventType.FILE_COMPLETED, toNode, toChannel, fileName, fileType, exchangeId, ChannelListener.SENT));
        }
    }

    @Override
    public void onFileWillReceive(String fromNode, String fromChannel, String fileName,
            String hash, String fileType, String exchangeId, long fileSize) {
        if(fromChannel.equals(CHORD_APPLICATION_CHANNEL)) {
            if(exchangeId !=null) {
                fileTypes.put(exchangeId, fileType);
            }
            File targetdir = new File(CHORD_FILE_PATH);
            if (!targetdir.exists()) {
                targetdir.mkdirs();
            }

            // Because the external storage may be unavailable,
            // you should verify that the volume is available before accessing it.
            // But also, onFileFailed with ERROR_FILE_SEND_FAILED will be called while Chord got failed to write file.
            StatFs stat = new StatFs(CHORD_FILE_PATH);
            long blockSize = stat.getBlockSize();
            long totalBlocks = stat.getAvailableBlocks();
            long availableMemory = blockSize * totalBlocks;

            if (availableMemory < fileSize) {
                rejectFile(exchangeId);
                dispatchEvent(new FileEvent(EventType.FILE_COMPLETED, fromNode, fromChannel, fileName, fileType, exchangeId, ChannelListener.FAILED));
                return;
            }
            dispatchEvent(new FileEvent(EventType.FILE_WILL_RECEIVE, fromNode, fileName, fileType, exchangeId));
        }
    }

    @Override
    public void onNodeJoined(String node, String channel) {
        if(channel.equals(CHORD_APPLICATION_CHANNEL)) {
            dispatchEvent(new NodeEvent(EventType.NODE_JOINED, node, channel));
        }
    }

    @Override
    public void onNodeLeft(String node, String channel) {
        if(channel.equals(CHORD_APPLICATION_CHANNEL)) {
            dispatchEvent(new NodeEvent(EventType.NODE_LEFT, node, channel));
        }
    }

    // Reject to receive file.
    public boolean rejectFile(String exchangeId) {
        return mAppChannel.rejectFile(exchangeId);
    }

    public String sendFile(String toNode, String filePath, String fileType) {
        String exchangeId = mAppChannel.sendFile(toNode, fileType, filePath, NetworkManager.SHARE_FILE_TIMEOUT_MILISECONDS);
        if(exchangeId != null) {
            fileTypes.put(exchangeId, fileType);
        }
        return exchangeId;
    }

}
