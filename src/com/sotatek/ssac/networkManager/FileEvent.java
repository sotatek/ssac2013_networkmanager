package com.sotatek.ssac.networkManager;

public class FileEvent extends Event{
	private String node, fileName, fileTye, exChangeId;
	private String deviceId;
	boolean isSend;
	int reason, progress;
	FileEvent(EventType type, String node, String fileName, String fileType, String exChangeId) {
		this.node = node;
		this.fileName = fileName;
		this.fileTye = fileType;
		this.exChangeId = exChangeId;
		this.type = type;
	}
	FileEvent(EventType type, String node, String channel, String fileName, String fileType, String exChangeId, int reason) {
		this(type, node,fileName, fileType, exChangeId);
		this.reason = reason;
	}
	
	public EventType getType() {
		return type;
	}

	public void setType(EventType type) {
		this.type = type;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}
	
	public int getProgress() {
	    return progress;
	}
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileTye() {
        return fileTye;
    }
    public void setFileTye(String fileTye) {
        this.fileTye = fileTye;
    }
    public String getExChangeId() {
		return exChangeId;
	}

	public void setExChangeId(String exChangeId) {
		this.exChangeId = exChangeId;
	}

	public int getReason() {
		return reason;
	}

	public void setReason(int reason) {
		this.reason = reason;
	}
    public String getDeviceId() {
        return deviceId;
    }
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
